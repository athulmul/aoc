# input   =["1-3 a: abcde",
# "1-3 b: cdefg",
#   "1-4 c: cccccc"]

# print(input)

validcounter=0

input = open('input.txt').read().splitlines()

import re
from operator import xor

for i in input:

  pos12, alphabet, pwd=i.split()

  pos12 = re.findall('(\d+-\d+)', pos12)
  pos12=pos12[0].split('-')

#   print()
#   print('pos12')
#   print(pos12)

  alphabet=alphabet[0]
  
#   print()
#   print('alphabet')
#   print(alphabet)

#   print()
#   print('pwd')
#   print(pwd)
#   print('pwd pos')
#   print(pwd[int(pos12[0])- 1 ], pwd[int(pos12[1]) - 1])

  if xor(pwd[int(pos12[0])-1] == alphabet, pwd[int(pos12[1])-1] == alphabet):
      validcounter+=1



print('counter')
print(validcounter)