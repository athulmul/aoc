input = open('input.txt').read().splitlines()

import json

RESULT_DICT = json.load(open('output.json', 'r+'))

for i in input:
    # import ipdb;ipdb.set_trace()
    instruction, result = i.split('->')

    instruction = instruction.strip()
    result = result.strip()

    if 'AND' in instruction:
        value1, value2 = instruction.split('AND')
        value1 = value1.strip()
        value2 = value2.strip()
    
        try:
            RESULT_DICT[result] = RESULT_DICT[value1] & RESULT_DICT[value2]
        except KeyError:
            continue
    elif 'OR' in instruction:
        value1, value2 = instruction.split('OR')
        value1 = value1.strip()
        value2 = value2.strip()

        try:
            RESULT_DICT[result] = RESULT_DICT[value1] | RESULT_DICT[value2]
        except KeyError:
            continue
    elif 'LSHIFT' in instruction:
        value1, value2 = instruction.split('LSHIFT')
        value1 = value1.strip()
        value2 = value2.strip()

        try:
            RESULT_DICT[result] = RESULT_DICT[value1] << int(value2)
        except KeyError:
            continue
    elif 'RSHIFT' in instruction:
        value1, value2 = instruction.split('RSHIFT')
        value1 = value1.strip()
        value2 = value2.strip()

        try:
            RESULT_DICT[result] = RESULT_DICT[value1] >> int(value2)
        except KeyError:
            continue

    elif 'NOT' in instruction:
        # import ipdb; ipdb.set_trace()

        value1, value2 = instruction.split('NOT')
        value1 = value1.strip()
        value2 = value2.strip()
        try:
            RESULT_DICT[result] = ~RESULT_DICT[value2] if ~RESULT_DICT[value2] > 0 else ~RESULT_DICT[value2] + 2**16
        except KeyError:
            continue
    else:
        # assignment
        # import ipdb;ipdb.set_trace()

        try:
            RESULT_DICT[result] = int(instruction)
        except ValueError:
            continue
json.dump(RESULT_DICT, open('output.json','w+'))

print(RESULT_DICT)
